import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { User } from '../../shared/models/auth/user.model';
import Account from '../../shared/models/auth/account.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly apiPath = environment.apiBaseUrl + '/user';
  expireDate: number;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    const storedExpireDate = localStorage.getItem('expire');
    if (storedExpireDate) {
      this.expireDate = +storedExpireDate;
    }
   }

  registerAccount(account: User): Observable<any> {
    return this.http.post<User>(this.apiPath, account);
  }

  loginAccount(account:Account): Observable<any> {
    return this.http
    .get(this.apiPath+ `/login?username=${account.username}&password=${account.password}`, {observe: 'response'})
    .pipe(map(() => this.storeExpireDate()));
  }

  storeExpireDate() {
    this.expireDate = Date.now() + 1000 * 60 * 60;
    localStorage.setItem('expire', `${this.expireDate}`);
    return this.expireDate;
  }

  logout() {
    this.expireDate = null;
    localStorage.removeItem('expire');
    this.router.navigate(['/login']);
  }

  isValidSession(): boolean {
    if (this.expireDate === null || this.expireDate === undefined) {
      return false;
    }
    
    if (this.expireDate && this.expireDate < Date.now()) {
      this.expireDate = null;
      return false;
    }
    return true;
  }
}
