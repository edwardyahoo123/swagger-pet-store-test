import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PetCreateComponent } from './pages/pet-create/pet-create.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageUploaderComponent } from './components/image-uploader/image-uploader.component';



@NgModule({
  declarations: [PetCreateComponent, ImageUploaderComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class PetCreateModule { }
