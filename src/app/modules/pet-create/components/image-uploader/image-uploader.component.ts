import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {
  @Output('changeFormData') changeFormDataEmitter = new EventEmitter<File[]>();;
  urls: string[] = []
  fomrData: File[] =[];


  constructor() { }

  ngOnInit(): void {
  }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      const filesLength = event.target.files.length;

      for(let i = 0; i < filesLength; ++i) {
        const reader = new FileReader();
        
        this.fomrData.push(event.target.files[i]);
        reader.readAsDataURL(event.target.files[i]); // read file as data url

        reader.onload = (event) => { // called once readAsDataURL is completed
          this.urls.push(event.target.result as string)
          console.log(this.urls)
        }
      }
      this.changeFormDataEmitter.emit(this.fomrData);
    };
  }

  // onSelectFile(event) { // called each time file input changes
  //   if (event.target.files && event.target.files[0]) {
  //     var reader = new FileReader();
  //     this.formData = event.target.files[0];
  //     reader.readAsDataURL(event.target.files[0]); // read file as data url

  //     reader.onload = (event) => { // called once readAsDataURL is completed
  //       this.url = event.target.result;
  //     }
  //   }
  // }
}
