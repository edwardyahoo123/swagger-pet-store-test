import { Component, ComponentFactoryResolver, ElementRef, OnInit, ViewChild } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms'
import { MatChipInputEvent } from '@angular/material/chips';
import { Pet, PetCategory } from '../../../../shared/models/pet/pet.model';
import { PetStoreService } from '../../../../services/pet-store.service';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pet-create',
  templateUrl: './pet-create.component.html',
  styleUrls: ['./pet-create.component.scss']
})
export class PetCreateComponent implements OnInit {
  // Form values
  pet: Pet;
  
  // Tags Input
  tags: string[] = [];
  tagCtrl = new FormControl();
  separatorKeysCodes: number[] = [ENTER, COMMA];

  // Image Upload
  @ViewChild('fileInput') fileInput: ElementRef;
  fileAttr = 'Choose File'
  url: any;
  formData: File[];

  constructor(
    private petStoreSrevice: PetStoreService,
    private router: Router,
  ) {
    this.pet = new Pet();
    this.pet.id = Date.now();
    this.pet.name = '';
    this.pet.category = new PetCategory(this.pet.id + 1, '');
  }

  ngOnInit(): void {
  }

  removeTag(index): void {
    this.tags = this.tags.filter((tag, idx) => idx !== index);
  }

  addTag(event: MatChipInputEvent):void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }

    if (input) {
      input.value = '';
    }

    this.tagCtrl.setValue(null);
  }

  changeUploadImagesHandler(event) {
    this.formData = event;
  }

  submitFormHandler() {
    console.log(this.pet.name)
    if (this.pet.name === '' || this.pet.category.name === '') {
      return;
    }

    forkJoin(
      this.formData.map(data => this.petStoreSrevice.uploadImage(1617101436273,data))
    ).subscribe(() => {
      this.pet.tags = this.tags.map((tag, idx) => {
        return {
          name: tag,
          id: idx
        }
      });
      this.pet.status = 'available';
  
      this.petStoreSrevice.createPet(this.pet).subscribe(res => {
        this.router.navigate(['/']);
      })
    })
  }
}
