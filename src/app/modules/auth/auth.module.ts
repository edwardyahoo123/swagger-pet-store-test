import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { SharedModule } from '../../shared/shared.module';
import { AuthLayoutComponent } from './component/auth-layout/auth-layout.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [LoginComponent, RegisterComponent, AuthLayoutComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
  ],
  // schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthModule { }
