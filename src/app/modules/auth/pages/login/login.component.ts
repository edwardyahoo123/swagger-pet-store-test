import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/auth/auth.service';
import Account from '../../../../shared/models/auth/account.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  account = new Account('', '');

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  submitLoginFormHandler() {
    if (this.account.username === '' || this.account.password === '') {
      return;
    }

    this.authService.loginAccount(this.account).subscribe(() => {
      this.router.navigate(['/']);
    })
  }

}
