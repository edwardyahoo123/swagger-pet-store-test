import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/auth/auth.service';
import { User } from '../../../../shared/models/auth/user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  newUserAccount: User = new User();
  registerSuccess: boolean = false;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  submitRegistrationFormHandler() {
    this.newUserAccount.id = 0;
    this.newUserAccount.userStatus = 0;
    this.authService.registerAccount(this.newUserAccount).subscribe(
      res => {
      // console.log(res);
      this.registerSuccess = true;
    },
    err=> {
      console.log(err);
    }
    )
  }
}
