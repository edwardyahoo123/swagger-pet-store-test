import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PetListComponent } from './pages/pet-list/pet-list.component';
import { PetCardComponent } from './components/pet-card/pet-card.component';
import { SharedModule } from '../../shared/shared.module';
import { PetDetailDialogComponent } from './components/pet-detail-dialog/pet-detail-dialog.component';
import { CreateFabComponent } from './components/create-fab/create-fab.component';



@NgModule({
  declarations: [PetListComponent, PetCardComponent, PetDetailDialogComponent, CreateFabComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class PetListModule { }
