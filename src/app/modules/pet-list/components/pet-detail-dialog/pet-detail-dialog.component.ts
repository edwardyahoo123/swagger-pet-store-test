import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Pet } from '../../../../shared/models/pet/pet.model';

@Component({
  selector: 'app-pet-detail-dialog',
  templateUrl: './pet-detail-dialog.component.html',
  styleUrls: ['./pet-detail-dialog.component.scss']
})
export class PetDetailDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public pet: Pet
  ) { }

  ngOnInit(): void {
  }

}
