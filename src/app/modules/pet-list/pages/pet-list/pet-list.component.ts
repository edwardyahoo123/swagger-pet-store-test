import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '../../../../core/auth/auth.service';
import { PetStoreService } from '../../../../services/pet-store.service';
import { Pet } from '../../../../shared/models/pet/pet.model';
import { PetDetailDialogComponent } from '../../components/pet-detail-dialog/pet-detail-dialog.component';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.scss']
})
export class PetListComponent implements OnInit {
  pets: Pet[] = [];

  constructor(
    private petStoreService: PetStoreService,
    private authService: AuthService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.petStoreService.getPets().subscribe(res => {
      console.log(res)
      this.pets = res;
    })
  }

  clickPetHandler(pet) {
    this.dialog.open(PetDetailDialogComponent, {
      width: '300px',
      data: pet
    });
  }

  clickLogoutButtonHandler() {
    this.authService.logout();
  }
}
