export class User {
  constructor(
    public id?: number | string,
    public username?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public password?: string,
    public phone?: string,
    public userStatus?: number
  ){}
}