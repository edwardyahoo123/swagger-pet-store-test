export class Pet {
  constructor(
    public id?: number,
    public category?: PetCategory,
    public name?: string,
    public photoUrls?: string[],
    public tags?: Tag[],
    public status?: 'available' | 'pending' | 'sold'
  ){}
}

export class PetCategory {
  constructor(
    public id?: number,
    public name?: string,
  ){}
}

export class Tag {
  constructor(
    public id?: number,
    public name?: string,
  ){}
}