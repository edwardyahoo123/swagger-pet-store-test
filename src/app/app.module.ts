import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from './modules/auth/auth.module';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { PetListModule } from './modules/pet-list/pet-list.module';
import { PetCreateModule } from './modules/pet-create/pet-create.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // modules
    AuthModule,
    CoreModule,
    PetListModule,
    PetCreateModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
