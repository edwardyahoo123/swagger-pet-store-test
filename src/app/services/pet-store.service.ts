import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Pet } from '../shared/models/pet/pet.model';

@Injectable({
  providedIn: 'root'
})
export class PetStoreService {
  private readonly apiPath = environment.apiBaseUrl + '/pet/'

  constructor(
    private http: HttpClient,
  ) { }

  uploadImage(petId: number, formData:File): Observable<any> {
    const newFormData = new FormData();
    newFormData.append('file', formData, formData.name);
    
    return this.http.post(this.apiPath + `${petId}/uploadImage`,newFormData);
  }

  createPet(pet: Pet): Observable<any> {
    return this.http.post(this.apiPath, pet);
  }

  getPets(): Observable<any[]> {
    return this.http.get<any[]>(this.apiPath + 'findByStatus?status=available');
  }
}
